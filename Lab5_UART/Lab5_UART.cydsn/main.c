/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
extern char disp1[];
extern char disp2[];
extern char disp3[];
extern char disp4[];
extern char eeprom[];
extern volatile int flag;   
int check = 0;

char disp1[17];
char disp2[17];
char disp3[17];
char disp4[17];
char eeprom[17];

char blocking_char() //when input != 0 function exits and returns a char
{
    char input = 0;
    
    while(input == 0) 
    {
        input = UART_GetChar(); // get data if input not 0
        if (flag == 1) //erase all string data
        {
            LCD_ClearDisplay();
            memset(disp1, 0, 17);                                   //reset string data to 0s
            memset(disp2, 0, 17);
            memset(disp3, 0, 17);
            memset(disp4, 0, 17);
            EEPROM_EraseSector(0);                                  //erase sector of eeprom which contains 4 rows of data
            UART_PutString("\r\nThe strings have been cleared");
            check = 0;                                              //send message again
            flag = 0;                                               //reset flag
        }        
    }
    return input;
}


void save(char* inputArray)
{
    int i = 0;
    while(i < 100)
    {
        char in = blocking_char();
        if (in != 0x0D) //if not carriage return
        { 
            
            if (((in == 0x08) || (in == 0x7f)) && i >= 1) //if BS or DEL pressed, rubout sequence
            {
                UART_PutChar(0x08); //rubout sequence to realterm
                UART_PutChar(0x20);
                UART_PutChar(0x08);
                i = i -1; //move back 1 and write over the BS or DEL
                inputArray[i] = '\0';
            }
            else if (((in == 0x08) || (in == 0x7f))&& i == 0) //if BS or DEL pressed, rubout sequence
            {
                i = 0;                                      //do nothing besides reset counter for more chars to be entered
                UART_PutChar(0x07);                         //put bel character if trying to backspace without inputting anything first
                inputArray[i] = '\0';   
            }
            else if (i >= 15)
            {
                inputArray[i] = '\0';
                UART_PutChar(0x07); //beep at user once 16 writeable chars reached
            }
            else
            {
                inputArray[i] = in;                         //save the data to the array
                UART_PutChar(in);                           //display to Realterm the character
                i++;
            }
        }
        else if (in == 0x0D) //if input enter, terminate string
        {
            inputArray[i] = '\0';
            i = 100; //exit function
        }
    }
}

void read_row(char* array, uint16 address) //reads 16 bytes from row address
{
    for (int i = 0; i <= 15; i++)
    {
        array[i] = EEPROM_ReadByte((address) + i);
    }
}

void string_write(char * array, uint16 address) //select array and address for eeprom write memory
{
    UART_PutString("\r\nEnter string: ");
    save(array);
    EEPROM_Write((const uint8*)array,address);
}

void string_read_lcd(char* array, uint16 address) //select array and address for eeprom read memory
{
    LCD_ClearDisplay();
    read_row(array,address);
    LCD_Position(0,0);
    LCD_PrintString(array);
}
void string_read_uart(char* array, uint16 address)
{
    read_row(array,address);
    UART_PutString(array);
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    CLR_STR_ISR_Start();
    CLR_STR_ISR_Enable();
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    LCD_Start();
    EEPROM_Start();
    UART_Start();
    memset(disp1, 0, 17);
    memset(disp2, 0, 17);
    memset(disp3, 0, 17);
    memset(disp4, 0, 17);
    char userInput = 0;
    char string_num = 0;
    flag = 0;
    
    for(;;)
    {
        if (check == 0) 
        {
            UART_PutString("\r\nEnter (e) or Display (d)?: ");
            check = 1;
        }
        
        userInput = blocking_char();
        UART_PutChar(userInput);
        if (userInput == 'e') 
        {
            check = 0;                                              //reset check to send message again after enter/display action occurs
            UART_PutString("\r\nEnter string # (1 through 4): ");
            string_num = blocking_char();
            UART_PutChar(string_num);
            if (string_num == '1')  {
                string_write(disp1,0); //which array and which row of EEPROM to write to
                UART_PutString("\r\nEntered: ");
                UART_PutString(disp1);
                UART_PutString("\r\n");
            }
            else if (string_num == '2') string_write(disp2,1);
            else if (string_num == '3') string_write(disp3,2);
            else if (string_num == '4') string_write(disp4,3);
        }
        else if (userInput == 'd') UART_PutString("\r\nLCD (L) or UART (U)?: ");
        else if (userInput == 'L')
        {
            check = 0;
            UART_PutString("\r\nEnter string # (1 through 4): ");
            string_num = blocking_char();
            UART_PutChar(string_num);
            if (string_num == '1')      string_read_lcd(eeprom,0); //which array and which byte of EEPROM to start reading from
            else if (string_num == '2') string_read_lcd(eeprom,16);
            else if (string_num == '3') string_read_lcd(eeprom,32);
            else if (string_num == '4') string_read_lcd(eeprom,48);
        }
        else if (userInput == 'U')
        {
            check = 0;
            UART_PutString("\r\nEnter string # (1 through 4): ");
            string_num = blocking_char();
            UART_PutChar(string_num);
            UART_PutChar(0x0D);                                     //put CR & LF for readability on Realterm
            UART_PutChar(0x0a);
            if (string_num == '1')      string_read_uart(eeprom,0); //which array and which byte of EEPROM to start reading from
            else if (string_num == '2') string_read_uart(eeprom,16);
            else if (string_num == '3') string_read_uart(eeprom,32);
            else if (string_num == '4') string_read_uart(eeprom,48);
        }
        else 
        {
            UART_PutString("Invalid Input. Try Again.\r\n");
            check = 0;
        }
    }
}
/* [] END OF FILE */
